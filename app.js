
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var clothesRouter = require('./routes/clothes');
const mongodbClient = require('./mongo.connector')
const app = require('express')()

const bodyParser = require('body-parser')
mongodbClient.init()
.then(client =>{
    app.use(bodyParser.json())
    app.use('/users', usersRouter)
    app.use('/home', indexRouter)
    app.use('/clothes', clothesRouter)
    app.listen(9999, () => {
        console.log('App listening on port 9999')
    })
}).catch(err=>{
    throw err
})

