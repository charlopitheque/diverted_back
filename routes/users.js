var express = require('express');
var router = express.Router();
const connector = require('./../mongo.connector')
var ObjectId = require('mongodb').ObjectID;

/* GET users listing. */
router.get('/', function(req, res, next) {
  connector.db.collection('users').find({}).toArray((err, result)=>{
    if (err) throw err
    res.send(result)
  });
});

/* GET user by id */ 
router.get('/:id', (req, res, next) => {
  console.log({params:req.params});
  connector.db.collection('users').find({"_id": ObjectId(req.params.id)}).toArray((err, results)=>{
    console.log(results); // output all records
    res.send(err||results)
  })
});

/** CREATE a new User */
router.post('/', async (req, res, next)=>{
  let newUser = await connector.db.collection('users').insertOne(
    { 
      name : req.body.name,
      lastname : req.body.lastname,
      email : req.body.email,
      password : req.body.pwd,
      previous_purchases : [],
      roles : req.body.roles
    }
  )
  res.send(newUser)
});

/** DELETE a user */
router.delete('/:id', (req, res) => {
  let myquery = {"_id": ObjectId(req.params.id) };
  connector.db.collection("users").deleteOne(myquery, (err, obj) => {
    if (err) throw err
    res.send(obj)
  });
});


module.exports = router;
