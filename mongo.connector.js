const MongoClient = require('mongodb').MongoClient
let username = 'dev'
let pwd = encodeURIComponent('azerty44')
//connection url
const url = `mongodb://${username}:${pwd}@10.44.100.41:27017`

// database name
const dbName = 'diverted_dev'

//create new mongo client
const client = new MongoClient(url+'/'+dbName,{useNewUrlParser:true, sslValidate: false, ssl: true})

class MongoConnector{
    init(){
        return new Promise((resolve, reject)=>{
            client.connect()
            .then(connectedClient => {
                this.client = connectedClient
                this.db = connectedClient.db(dbName)
                console.log("Connected succesfully")
                resolve(connectedClient)
            }).catch(err=>{ 
                console.error('Failed to connect')
                throw err
            })
        })
    }
}
const connector = new MongoConnector()
module.exports = connector