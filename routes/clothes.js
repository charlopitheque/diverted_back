var express = require('express');
var router = express.Router();
const connector = require('../mongo.connector')


/**
 * GET all
 */
<<<<<<< HEAD
router.get('/', (req, res) => {

    connector.db.collection("clothes").find({}).toArray((err, result) => {
        if (err) throw err;
        res.send(result)
    });

});

/**
 * GET by id
 */
router.get('/:clothingId', (req, res) => {

    connector.db.collection("clothes").find({
        "_id": id
    }).toArray((err, result) => {
        if (err) throw err;
        res.send(result)
    });

});

/**
 * GET by size
 */
router.get('/size/:clothingSize', (req, res) => {

    connector.db.collection("clothes").find({
        "size": clothingSize
    }).toArray((err, result) => {
        if (err) throw err;
        res.send(result)
    });

});


/**
 * DELETE by id
 */
router.delete('/:clothingId', (req, res) => {

    //On récupère l'id et on le parse en integer
    let id = parseInt(req.params.clothingId);

    //Ligne pour supprimer un vetement
    connector.db.collection("clothes").deleteOne({
        "_id": id
    });

    //On sélectionne tous les vetements pour vérifier la suppression
    connector.db.collection("clothes").find({}).toArray((err, result) => {
=======
router.get('/',  (req, res, next)=> {
    connector.db.collection("clothes").find({}).toArray((err, result)=> {
>>>>>>> 74ca24f713273b0d9df799d7e9965043452e4f93
        if (err) throw err;
        res.send(result)
    });

});


/**
 * INSERT one
 */
router.post('/', (req, res) => {

    let newClothesName = req.body.name
    let newClothesPrice = req.body.price
    let newClothesDesc = req.body.desc
    let newClothesSize = req.body.clothing_size
    let newClothesColor = req.body.clothing_color
    let newClothesType = req.body.clothing_type

    //On créé la nouvelle liste a ajouter en suivant le model du JSON qui contient tout
    var newItem = {
        "name": newClothesName,
        "price": newClothesPrice,
        "description": newClothesDesc,
        "reviews": [],
        "size": newClothesSize,
        "color": newClothesColor,
        "clothing": newClothesType,
        "promotion": {}
    }

    //On ajoute la liste précédemment créée
    connector.db.collection('clothes').insertOne(newItem)
        .catch((err) => {
            throw err
        });

    //On affiche la liste après ajout
    connector.db.collection("clothes").find({}).toArray((err, result) => {
        if (err) throw err;
        res.send(result)
    });
})


/**
 * PUT one
 */
router.put('/', (req, res) => {

    let updateAttribute = req.body.attribute
    let updateValue = req.body.value
    let updateId = req.body.id

    const setData = {}
    setData[updateAttribute] = updateValue

    //On modifie la liste
    connector.db.collection("clothes").updateOne({
        "_id": updateId
    }, {
        $set: setData
    });

    //On sélectionne toutes les listes pour vérifier la modification
    connector.db.collection("clothes").find({}).toArray((err, result) => {
        if (err) throw err;
        res.send(result)
    });
})



module.exports = router;